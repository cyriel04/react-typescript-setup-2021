module.exports = {
  singleQuote: true,
  semi: false,
  trailingComma: 'es5',
  jsxSingleQuote: false,
  endOfLine: 'auto',
  // tabWidth: 4,
}
