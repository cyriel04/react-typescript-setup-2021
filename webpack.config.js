const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const webpack = require('webpack')
const { version } = require('./package.json')

const isDev = process.env.NODE_ENV === 'development'

console.log(isDev, version)
module.exports = {
  entry: './src/index.tsx',
  output: {
    publicPath: '/',
    path: path.join(__dirname, '/build'),
    filename: 'bundle.js',
  },
  devtool: isDev ? 'cheap-module-source-map' : 'nosources-source-map',
  devServer: isDev
    ? {
        historyApiFallback: true,
        host: '0.0.0.0',
        port: 8080,
        static: __dirname,
        devMiddleware: {
          publicPath: '/',
        },
        client: { overlay: false },
      }
    : undefined,
  resolve: {
    modules: ['node_modules'],
    mainFiles: ['index', 'script', 'styles'],
    extensions: ['.ts', '.tsx', '.js'],
    alias: {
      components: path.resolve(__dirname, 'src/components'),
    },
  },
  module: {
    rules: [
      {
        test: /\.(ts|js)x$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
          },
        ],
      },
      {
        test: /\.scss$/,
        use: [
          'style-loader',
          'css-loader',
          {
            loader: 'sass-loader',
          },
        ],
      },
      {
        test: /\.(jpe?g|gif|png|ico)$/,
        type: 'asset/resource',
      },
      {
        test: /\.(woff(2)?|eot|ttf|otf|svg|)$/,
        type: 'asset/inline',
      },
    ],
  },
  mode: isDev ? 'development' : 'production',
  // optimization: {
  // 	minimize: true,
  // 	splitChunks: {
  // 		chunks: 'all',
  // 		cacheGroups: {
  // 			base: {
  // 				test: /[\\/]node_modules[\\/]/,
  // 				name: 'base',
  // 				chunks: 'all',
  // 				reuseExistingChunk: true
  // 			}
  // 			// components: {
  // 			// 	test: /[\\/](components)[\\/]/,
  // 			// 	name: 'components',
  // 			// 	chunks: 'all',
  // 			// 	reuseExistingChunk: true
  // 			// }
  // 		}
  // 	}
  // },

  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html',
      version,
      env: isDev ? 'development' : 'production',
    }),
    new webpack.DefinePlugin({
      process: { env: { VERSION: JSON.stringify(version) } },
    }),
  ],
}
