import AddIcon from './assets/EyeIcon'

import _Test from './assets/rezero.png'
import Logo from './assets/logo.svg'
import NavBar from './components/NavBar'
const App = () => {
  return (
    <div className="root">
      {/* <img src={Test} width="100px" /> */}
      <NavBar />
      <AddIcon />
      <br />
      <div
        className="content"
        // style={{
        // 	background: `url(${Test})`,
        // 	backgroundSize: 'cover'
        // }}
      >
        <img src={Logo} alt="Inc" width="200px" className="site-logo" />

        <br />
        {`${process.env.NODE_ENV} - 
								${process.env.VERSION}`}
      </div>
      {`© Unnamed 2021 - v${process.env.VERSION}`}
    </div>
  )
}

export default App
